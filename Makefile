target := verifiedpqcrypto

.PHONY: all clean test exe

all: exe #$(target).html

exe: **/*.hs Makefile
	stack install --local-bin-path=.

$(target).html: $(target).adoc **/*.hs *.css
	asciidoctor $(target).adoc -a stylesheet=asciidoctor.css

test:
	stack test

clean:
	rm -rf $(target).html .stack-work *.exe *.cabal
