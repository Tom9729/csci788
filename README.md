# Verified Post-Quantum Cryptography

This project uses LiquidHaskell (Haskell with refinement types)
to implement variants of the McEliece cryptosystem and attempt
to verify them for correctness.

## Environment Setup
**Note:** This project was tested in a Ubuntu 20.04.1 LTS 
environment. It should work in other environments (including
Windows) but they have not been tested.

Install the latest version of the Haskell Stack tool (2.7.3) as of this writing, then run `make`. This will download and 
install the correct versions of GHC (the Haskell compiler) 
and LiquidHaskell, and then compile the code.

Run `make test` to verify the doctests are valid.

Currently there is no standalone executable; run `stack ghci` and then 
do `:l McElieceNiederreiter` to load the toy example, then open 
`lib/McElieceNiederreiter.hs` (the corresponding code) and you can
interactively evaluate expressions from the toy example. For example
evaluate `ptext == ptext'` to verify the input/output match.
