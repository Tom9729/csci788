This folder contains SAGE code from papers and [Github](https://github.com/davidhoo1988/Code_Based_Cryptography_Python) that I have used as a reference implementation. The Github code is GPL licensed from David Hu 
and is based on the Risse and Roering papers with small modifications 
from me to make it work on the current version of Sage and to make 
the output similar to my toy examples.
