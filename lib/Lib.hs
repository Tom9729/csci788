module Lib where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

{-@ double :: x : Int -> {y : Int | y = x + x} @-}
double :: Int -> Int
double x = x * 2
