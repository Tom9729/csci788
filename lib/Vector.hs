{- |
Description : CSCI-788 Capstone Project - Vector operations.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

This module is based on the following LiquidHaskell tutorial:
https://ucsd-progsys.github.io/liquidhaskell-tutorial/Tutorial_07_Measure_Int.html;
specifically the basic functions and types; vGet and down is my
own work.
-}
module Vector where

import Base
import List

--{-@ LIQUID "--bscope" @-}

{-@ data Vector a = V { vDim :: Nat
                      , vElts :: ListN a vDim }
  @-}
{-@ type VectorNE a = {v:Vector a | vDim v > 0} @-}
{-@ type VectorN a N = {v:Vector a | vDim v = N} @-}
{-@ type VectorGE a N = {v:Vector a | vDim v >= N} @-}
{-@ type VectorLE a N = {v:Vector a | vDim v <= N} @-}
{-@ type VectorX a X = VectorN a {vDim X} @-}
data Vector a = V
  {
    vDim :: Int
  , vElts :: List a
  } deriving (Eq, Ord)

-- | Pretty-print vectors.
instance (Num a, Show a) => (Show (Vector a)) where
  show (V _ columns) = "| " ++ (unwords $ map (show) columns) ++ " |"

{-@ vEmp :: VectorN a 0 @-}
vEmp = V 0 []

{-@ vCons :: a -> x:Vector a -> VectorN a {vDim x + 1} @-}
vCons x (V n xs) = V (n + 1) $ x:xs

{-@ vHd :: VectorNE a -> a @-}
vHd (V _ (x:_)) = x

{-@ vTl :: VectorNE a -> [a] @-}
vTl (V _ (_:xs)) = xs

{-@ for :: v:Vector a -> (a -> b) -> VectorX b v @-}
for :: Vector a -> (a -> b) -> Vector b
for (V n xs) f = V n $ map' f xs

{-@ forN :: v:Vector a -> ((a,Int) -> b) -> VectorX b v @-}
forN (V n xs) f = V n $ map' f $ zipWith' (,) xs $ nseq n

{-@ vZipWith :: (a -> b -> c)
             -> xs:Vector a
             -> ys:VectorX b xs
             -> VectorX c xs
  @-}
vZipWith :: (a -> b -> c) -> Vector a -> Vector b -> Vector c
vZipWith f (V n xs) (V _ ys) = V n $ zipWith' f xs ys

-- | Perform the dot product of two vectors.
--
-- Examples:
--
-- >>> V 3 [1,2,3] `dotProduct` V 3 [4,5,6]
-- 32
{-@ dotProduct :: (Num a) => xs:Vector a -> VectorX a xs -> a @-}
dotProduct :: (Num a) => Vector a -> Vector a -> a
dotProduct a b = sum $ zipWith' (*) (vElts a) (vElts b)

{-@ vGet :: v:VectorNE a
         -> i:{Nat | i < vDim v}
         -> a @-}
vGet :: Vector a -> Int -> a
vGet v@(V n xs) i =
  let (x:_) = drop' i xs
  in x

{-@ vSwap :: v:Vector a
          -> i:{Nat | i < vDim v}
          -> j:{Nat | j < vDim v}
          -> VectorX a v @-}
vSwap :: Vector a -> Int -> Int -> Vector a
vSwap v@(V n xs) i j = V n $ go xs 0
  where
    {-@ go :: xs:List a
           -> k:Nat
           -> ys:ListX a xs @-}
    go [] _ = []
    go (x:xs) k =
      let x' = if k == i then vGet v j
               else if k == j then vGet v i
                    else x
      in x' : (go xs $ k + 1)

{-@ vFindN :: v:VectorNE a
           -> (a -> Bool)
           -> n:{Nat | n < vDim v}
           -> i:{Maybe Nat | MaybeLT i (vDim v)} @-}
vFindN :: Vector a -> (a -> Bool) -> Int -> Maybe Int
vFindN v@(V _ xs) f n = findN xs f n

{-@ vDrop :: v:Vector a
          -> n:{Nat | n < vDim v}
          -> v':{Vector a | vDim v' <= vDim v &&
                            vDim v' = vDim v - n} @-}
vDrop :: Vector a -> Int -> Vector a
vDrop (V l xs) n = V (size xs') xs'
  where xs' = (drop' n xs)

{-@ vDropWhile :: (a -> Bool)
               -> v:Vector a
               -> Vector a @-}
vDropWhile :: (a -> Bool) -> Vector a -> Vector a
vDropWhile f v@(V _ xs) = V (size xs') xs'
  where
    xs' = dropWhile' f xs

{-@ vTake :: v:Vector a
          -> n:{Pos | n < vDim v}
          -> v':VectorN a n @-}
vTake :: Vector a -> Int -> Vector a
vTake (V l xs) n = V (size xs') xs'
  where xs' = (take' n xs)

{-@ vConcat :: u:Vector a
            -> v:Vector a
            -> w:{Vector a | vDim w = (vDim u + vDim v)} @-}
vConcat :: Vector a -> Vector a -> Vector a
vConcat u@(V un us) v@(V vn vs) =
  V (un + vn) $ concat' us vs

-- | Extend one vector to be the same length as another.
--
-- >>> vAlign (V 3 [1,1,1]) (V 4 [1,0,0,0])
-- | 0 1 1 1 |
{-@ vAlign :: (Num a)
           => p:Vector a
           -> q:Vector a
           -> r:{Vector a | if (vDim p) > (vDim q)
                            then (vDim r) = (vDim p)
                            else (vDim r) = (vDim q)}
  @-}
vAlign :: (Num a) => Vector a -> Vector a -> Vector a
vAlign p@(V pd _) q@(V qd _) = p'
  where
    d = max pd qd
    p' = vExtend p d

-- | Extend a vector to a certain number of elements by padding
-- it to the left with zeros.
--
-- >>> vExtend (V 3 [1,1,1]) 4
-- | 0 1 1 1 |
{-@ vExtend :: (Num a)
            => p:Vector a
            -> n:Nat
            -> q:{Vector a | if (vDim p) > n
                             then q = p
                             else (vDim q) = n}
  @-}
vExtend :: (Num a) => Vector a -> Int -> Vector a
vExtend p@(V pd ps) n =
  let dx = n - vDim p
  in
    if dx > 0
    then
      let p0 = V dx $ map' (\_ -> 0) $ nseq dx
      in
        vConcat p0 p
    else
      p

-- | Extend a vector to a certain number of elements by padding
-- it to the right with zeros.
--
-- >>> vExtendRight (V 3 [1,1,1]) 4
-- | 1 1 1 0 |
{-@ vExtendRight :: (Num a)
                 => p:Vector a
                 -> n:Nat
                 -> q:{Vector a | if (vDim p) > n
                                  then q = p
                                  else (vDim q) = n}
  @-}
vExtendRight p n =
  let dx = n - vDim p
  in
    if dx > 0
    then
      let p0 = V dx $ map' (\_ -> 0) $ nseq dx
      in
        vConcat p p0
    else
      p
