{- |
Description : CSCI-788 Capstone Project - McEliece Hamming Code implementation.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

The linear code used in McEliece is pluggable. Typically Goppa
codes are used which can correct multiple errors, but here we
use a Hamming code for simplicity; note that the result is verified
but not very secure due the properties of the Hamming code (i.e.
only single error correction).

McEliece algorithm described here:
http://www-math.ucdenver.edu/~wcherowi/courses/m5410/ctcmcel.html

Hamming code described here:
https://www.ece.unb.ca/tervo/ee4253/hamming2.shtml

TODO refactor into a library
-}
module McElieceHamming where

import BitMatrix
import Vector
import Matrix
import Bit
import List
import Coding
import Data.Maybe

-- | (Private) Hamming 7,4 code parity check matrix H.
{-@ parity :: MatrixN Bit 3 7 @-}
parity :: Matrix Bit
parity = M 3 7 $ V 3
  [
    V 7 [1, 1, 0, 1, 1, 0, 0]
  , V 7 [1, 0, 1, 1, 0, 1, 0]
  , V 7 [0, 1, 1, 1, 0, 0, 1]
  ]

-- | (Private) Generator matrix G derived from parity check matrix H.
-- https://en.wikipedia.org/wiki/Parity-check_matrix#Creating_a_parity_check_matrix
--
-- >>> generator
-- | 1 0 0 0 1 1 0 |
-- | 0 1 0 0 1 0 1 |
-- | 0 0 1 0 0 1 1 |
-- | 0 0 0 1 1 1 1 |
{-@ generator :: MatrixN Bit 4 7 @-}
generator :: Matrix Bit
generator = identity 4 `matAppend` (transpose $ takeColumns parity 4)
  
-- | (Private) Scrambler matrix S.
{-@ scrambler :: MatrixN Bit 4 4 @-}
scrambler :: Matrix Bit
scrambler = M 4 4 $ V 4
  [
    V 4 [1, 1, 0, 1]
  , V 4 [1, 0, 0, 1]
  , V 4 [0, 1, 1, 1]
  , V 4 [1, 1, 0, 0]
  ]
  
-- | (Private) Scrambler matrix inverse S^-1.
--
-- >>> scramblerInv
-- | 1 1 0 1 |
-- | 1 1 0 0 |
-- | 0 1 1 1 |
-- | 1 0 0 1 |
{-@ scramblerInv :: MatrixN Bit 4 4 @-}
scramblerInv :: Matrix Bit
scramblerInv = case bitMatInv scrambler of
  Just m -> m
  -- Default to identity if no error found.
  -- We should signal an error condition here instead.
  _ -> identity 4

-- | (Private) Permutation matrix P.
{-@ permutation :: MatrixN Bit 7 7 @-}
permutation :: Matrix Bit
permutation = M 7 7 $ V 7
  [
    V 7 [0, 1, 0, 0, 0, 0, 0]
  , V 7 [0, 0, 0, 1, 0, 0, 0]
  , V 7 [0, 0, 0, 0, 0, 0, 1]
  , V 7 [1, 0, 0, 0, 0, 0, 0]
  , V 7 [0, 0, 1, 0, 0, 0, 0]
  , V 7 [0, 0, 0, 0, 0, 1, 0]
  , V 7 [0, 0, 0, 0, 1, 0, 0]
  ]

-- | (Private) Permutation matrix inverse P^-1.
--
-- >>> permutationInv
-- | 0 0 0 1 0 0 0 |
-- | 1 0 0 0 0 0 0 |
-- | 0 0 0 0 1 0 0 |
-- | 0 1 0 0 0 0 0 |
-- | 0 0 0 0 0 0 1 |
-- | 0 0 0 0 0 1 0 |
-- | 0 0 1 0 0 0 0 |
{-@ permutationInv :: MatrixN Bit 7 7 @-}
permutationInv :: Matrix Bit
permutationInv = case bitMatInv permutation of
  Just m -> m
  -- Default to identity if no error found.
  -- We should signal an error condition here instead.
  _ -> identity 7

-- | (Public) Public generator matrix G'.
--
-- >>> generator'
-- | 1 1 1 1 0 0 0 |
-- | 1 1 0 0 1 0 0 |
-- | 1 0 0 1 1 0 1 |
-- | 0 1 0 1 1 1 0 |
{-@ generator' :: MatrixN Bit 4 7 @-}
generator' :: Matrix Bit
generator' = (scrambler `matProduct` generator) `matProduct` permutation

-- | Alice's message X.
{-@ ptext :: VectorN Bit 4 @-}
ptext :: Vector Bit
ptext = V 4 [1, 1, 0, 1]

-- | Error vector E introduced by Alice.
{-@ err :: VectorN Bit 7 @-}
err :: Vector Bit
err = V 7 [0, 0, 0, 0, 1, 0, 0]

-- | Cipher text Y.
-- >>> ctext
-- | 0 1 1 0 1 1 0 |
{-@ ctext :: MatrixN Bit 1 7 @-}
ctext :: Matrix Bit
ctext = (ptextM `matProduct` generator') `matAdd` errM
  where
    ptextM = matFromVec ptext
    errM = matFromVec err

-- | Bob calculates Y^-1.
-- >>> ctext'
-- | 1 0 0 0 1 1 1 |
{-@ ctext' :: MatrixN Bit 1 7 @-}
ctext' = ctext `matProduct` permutationInv

-- | Bob calculates the syndrome of the message.
-- Syndrome says where the errors are. Because we are using
-- Hamming code this can only reliably correct a single bit error.
-- >>> syndrome
-- | 0 |
-- | 0 |
-- | 1 |
{-@ syndrome :: MatrixN Bit 3 1 @-}
syndrome = parity `matProduct` (transpose ctext')

-- | Bob finds the error in the ciphertext.
{-@ err' :: MatrixN Bit 1 7 @-}
err' = case findError parity syndrome of
         Just m -> m
         -- Default to empty error if no error found.
         -- We should signal an error condition here instead.
         Nothing -> M 1 7 $ V 1 [V 7 [0,0,0,0,0,0,0]]

-- | Bob corrects the error in the ciphertext.
-- >>> ctext''
-- | 1 0 0 0 1 1 0 |
{-@ ctext'' :: MatrixN Bit 1 7 @-}
ctext'' = ctext' `matAdd` err'

-- | Bob drops the last 3 bits since they are the parity bits in this code.
-- >>> ctext'''
-- | 1 0 0 0 |
{-@ ctext''' :: MatrixN Bit 1 4 @-}
ctext''' = matFromVec $ V 4 $ take' 4 $ vElts $ matToVec ctext''

-- | Bob multiplies by S^-1 to get the original message.
-- >>> ptext'
-- | 1 1 0 1 |
ptext' = ctext''' `matProduct` scramblerInv
