{- |
Description : CSCI-788 Capstone Project - Galois field math.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

Fields are represented by a splitting polynomial.
Only binary fields are supported.

Field irreducible polynomial is a polynomial of bits. Field elements
consist of a reference to the irreducible polynomial as well as another
polynomial of bits.

One issue with this implementation is how we implemented several typeclasses to make working with polynomials
and field elements convenient, one of which allows coercing 0/1 to field elements. The problem here is that when this happens we have no context as to the field the element should
belong to. To workaround this we simply use whichever field is non-zero when working with
two elements (e.g. +/-/*/div).

Addition and subtraction are implemented as a bitwise AND. Multiplication is inherited from the
polynomial module except with an extra step to reduce by the field polynomial to keep the result
valid. Division is implemented as multiplication by the inverse, found by performing EEA with the
field polynomial.

This module is entirely my own work except for polynomial division which is due to Wikipedia. Galois field
math is based on knowledge and assignments from previous cryptography classes taken at RIT.

https://en.wikipedia.org/wiki/Polynomial_long_division#Pseudocode
https://www.ece.unb.ca/tervo/ece4253/poly.shtml
-}
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances, BangPatterns, FlexibleContexts #-}
module GaloisField where

import Bit
import Polynomial
import Vector
import Data.List (intercalate)
import List
import Data.Ratio
import Util
import Number

-- | Irreducible polynomial for the field.
type GF2Poly = Poly Bit

-- | Field element is an irreducible polynomial and the polynomial defining the value.
{-@ data GF2Element =
          E { eField :: f:{GF2Poly | f /= 0}
            , ePoly :: GF2Poly
            }
 @-}
data GF2Element = E
  {
    eField :: GF2Poly
  , ePoly :: GF2Poly
  } deriving (Ord)

-- TODO many of these impls set field to 0 when
-- converting from int, so the operations use whichever
-- field isn't zero but this is messy

-- | Implement number typeclass so we can use +/-/* with field elements.
instance Num GF2Element where
  (+) p@(E pf p') q@(E qf q') =
    E f $ addPoly p' q'
    where
      f = max pf qf -- handle field from int
      
  (-) p@(E pf p') q@(E qf q') =
    E f $ subPoly p' q'
    where
      f = max pf qf -- handle field from int
      
  (*) p q = multPolyGF2 p q
  
  abs p = p -- TODO
  signum p = p -- TODO
  fromInteger x = E 1 (P 0 $ V 1 [fromInteger x]) -- TODO

-- | For now only consider the polynomial when comparing equality between field elements,
--   ignoring the field polynomial.
instance Eq GF2Element where
  (==) a b = (ePoly a) == (ePoly b)

-- | Stubbed implementation of enum typeclass as requirement for integral.
-- TODO find cleaner way to do this
instance Enum GF2Element where
  toEnum x = E 0 $ P 0 $ V 1 [0] -- TODO
  fromEnum x = 0 -- TODO

-- | Stubbed implementation of real typeclass as requirement for integral.
-- TODO find cleaner way to do this
instance Real GF2Element where
  toRational x = 1 % 1 -- TODO

-- | Implement the integral typeclass so we can use division for field elements.
instance Integral GF2Element where
  quotRem a b = (divPolyGF2 a b, 0)
  toInteger a = 0 -- TODO

-- | Pretty-print field elements. The main distinction is we wrap
--   the output in parens and we use z instead of X.
instance {-# OVERLAPPING #-} Show (GF2Poly) where
  show (P d (V _ cs)) = "(" ++ (intercalate " + " $
                        filter (/= "") $
                        (go d cs)) ++ ")"
    where
      go :: Int -> [Bit] -> [[Char]]
      go _ (c:[]) = [if c /= 0 then (show c) else ""]
      go d (c:cs) =
        if c == 0
        then go (d - 1) cs
        else
          ((if c /= 1 then show c ++ "*" else "")
           ++ "z"
           ++ (if d /= 1 then "^" ++ show d else "")) : go (d - 1) cs

-- | Pretty-print field elements.
instance Show (GF2Element) where
  show (E f p) = show p

-- | Divide one field element by another by multiplying the first by the inverse of the second.
divPolyGF2 p@(E pf p') q@(E qf q') =
  multPolyGF2 p qInvE
  where
    f = max pf qf -- handle field from int
    (_, qInv, _) = extEuclidAlg q' f
    qInvE = E f qInv

-- | Multiply two field elements and reduce the result by the field polynomial.
multPolyGF2 p@(E pf p') q@(E qf q') =
  E f $ reduceGF2 (multPoly p' q') f
  where
    f = max pf qf -- handle field from int

-- | Reduce a polynomial by the field polynomial by repeatedly padding the field polynomial
--   to the right with zeroes and then ANDing them together until the result is of lesser degree.
{-@ reduceGF2 :: p:Poly a
              -> f:Poly a
              -> Poly a
  @-}
{-@ lazy reduceGF2 @-}
reduceGF2 p f@(P d v) =
  let delta = pDeg p - pDeg f
  in
    if delta < 0 || p == 0 || f == 1
    then p
    else
      let fd' = pDeg p
          f' = P fd' $ vExtendRight v $ fd' + 1
      in reduceGF2 (p + f') f

-- | Find the square root of a field element.
--
-- https://math.stackexchange.com/a/943456
--
-- Examples:
--
-- >>> f = P 3 $ V 4 [1, 0, 1, 1]
-- >>> p = E f $ P 2 $ V 3 [1, 0, 1]
-- >>> p
-- (z^2 + 1)
-- >>> sqrtGF2 p
-- (z + 1)
-- >>> (sqrtGF2 p)^2 == p
-- True
sqrtGF2 :: GF2Element -> GF2Element
sqrtGF2 p = p^2^(m - 1)
  where
    m = pDeg $ eField p
