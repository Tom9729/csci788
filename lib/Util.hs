{- |
Description : CSCI-788 Capstone Project - General utility functions.
Copyright : 2020 Tom Arnold <tca4384@rit.edu>
-}
{-# LANGUAGE StrictData, BangPatterns, ScopedTypeVariables #-}
module Util where

import Control.DeepSeq
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Parallel.Strategies
import Data.List
import System.CPUTime
import System.Environment
import System.Exit
import System.IO.Unsafe
import System.IO

-- | Process computation left-to-right (like Unix pipe).
--
-- Examples:
--
-- >>> [1,2,3] |> drop 1
-- [2,3]
(|>) :: a -> (a -> c) -> c
(|>) = flip ($)

-- | Like "|>" except applies to inside of elements. For example when
-- used with a list this will apply a function to each element.
--
-- Examples:
--
-- >>> [1,2,3] |>> (*2)
-- [2,4,6]
(|>>) :: Functor f => f a -> (a -> b) -> f b
(|>>) = flip (<$>)

-- | Like "|>>" except applied in parallel when compiled
-- in threaded mode and the appropriate RTS option are used (-Nx).
--
-- Examples:
--
-- >>> [1,2,3] ||>> (*2)
-- [2,4,6]
(||>>) xs f = (map f xs) `using` parList rdeepseq

-- | Run computation and return time in milliseconds and the result.
--
-- This is tricky because of lazy evaluation. We have to "force"
-- the expression to be evaluated completely because otherwise
-- the time will be 0ms.
--
-- Based on code from strict-io and timeit packages.
time :: (MonadIO m, NFData sa)
     => sa -- ^ Expression to be forced.
     -> m (Double, sa) -- ^ Time spent evaluating expression, and the expression.
time expr = do
  t1 <- liftIO getCPUTime
  let !value = force expr
  t2 <- liftIO getCPUTime
  let delta = fromIntegral (t2-t1) * 1e-9
  return (delta, value)

-- | Helper to print string (with newline) to standard error.
debugPutStrLn m = unsafePerformIO $ hPutStrLn stderr m
