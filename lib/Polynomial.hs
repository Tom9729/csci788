{- |
Description : CSCI-788 Capstone Project - Polynomial math.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

Polynomials are represented as a vector of coefficients.
Only univariate polynomials with positive degree are supported.

This is entirely my own work except for the polynomial inverse which is due to
Cohen (see Chapter 3 of Computational Algebraic Number Theory by Henri Cohen) and division
of polynomials which is due to Wikipedia. Additionally I used Sage as a reference by performing
the same operations in each to make sure mine was correct.

https://ebookcentral.proquest.com/lib/rit/reader.action?docID=3098147
https://en.wikipedia.org/wiki/Polynomial_long_division#Pseudocode
-}
{-# LANGUAGE BangPatterns #-}
module Polynomial where

import Vector
import List
import Data.List (intercalate)
import Data.Ratio
import Util
import Number

{-@ LIQUID "--bscope" @-}

{-@ data Poly a =
           P { pDeg :: Nat
             , pCoefs :: VectorN a {pDeg + 1}
             }
  @-}
{-@ type PolyN a N = {p:Poly a | pDeg p = N} @-}
data Poly a = P
  {
    pDeg :: Int
  , pCoefs :: Vector a
  } deriving (Eq, Ord)

-- | Pretty-print polynomials.
instance (Num a, Show a, Eq a) => (Show (Poly a)) where
  show (P d (V _ cs)) = intercalate " + " $
                        filter (/= "") $
                        go d cs
    where
      go :: (Num a, Show a, Eq a) => Int -> [a] -> [[Char]]
      go _ (c:[]) = [if c /= 0 then (show c) else ""]
      go d (c:cs) =
        if c == 0
        then go (d - 1) cs
        else
          ((if c /= 1 then show c ++ "*" else "")
           ++ "X"
           ++ (if d /= 1 then "^" ++ show d else "")) : go (d - 1) cs

-- | Implement number typeclass so we can use +/-/* and coerce
-- integers to polynomial constants.
instance (Eq a, Num a) => (Num (Poly a)) where
  (+) p q = addPoly p q
  (-) p q = subPoly p q
  (*) p q = multPoly p q
  abs p = p -- TODO
  signum p = p -- TODO
  fromInteger x = P 0 $ V 1 [fromInteger x]

-- | Stubbed implementation of enum typeclass as requirement for integral.
-- TODO find cleaner way to do this
instance (Num a, Enum a) => Enum (Poly a) where
  toEnum x = P 0 $ V 1 [0] -- TODO
  fromEnum x = 0 -- TODO

-- | Stubbed implementation of real typeclass as requirement for integral.
-- TODO find cleaner way to do this
instance (Real a) => Real (Poly a) where
  toRational x = 1 % 1 -- TODO

-- | Implement integral typeclass so we can divide polynomials.
instance (Integral a) => Integral (Poly a) where
  quotRem a b = quoRemPoly a b
  toInteger a = 0 -- TODO

-- | Apply an operation to pairs of coefficients.
--
-- >>> zipPolyWith (+) (P 2 $ V 3 [1,1,1]) (P 3 $ V 4 [1,0,0,1])
-- X^3 + X^2 + X + 2
--
-- >>> zipPolyWith (*) (P 2 $ V 3 [1,1,1]) (P 3 $ V 4 [1,0,0,1])
-- 1
{-@ zipPolyWith :: (Eq a, Num a)
                => (a -> a -> a)
                -> p:Poly a
                -> q:Poly a
                -> r:Poly a
  @-}
zipPolyWith :: (Eq a, Num a) => (a -> a -> a) -> Poly a -> Poly a -> Poly a
zipPolyWith f p@(P pd ps) q@(P qd qs) = P (pqd'' - 1) pqs''
  where
    ps' = vAlign ps qs
    qs' = vAlign qs ps
    pqs = vZipWith f ps' qs'
    pqs' = vDropWhile (==0) pqs
    pqd' = vDim pqs'
    {-@ pqs'' :: VectorNE a @-}
    pqs'' = if pqd' <= 0
            then V 1 [0]
            else pqs'
    pqd'' = vDim pqs''

-- | Get a coefficient of a polynomial.
--
-- >>> pCoef (P 2 $ V 3 [6,7,8]) 0
-- 6
{-@ pCoef :: (Num a)
          => p:Poly a
          -> i:{Nat | i <= pDeg p}
          -> a
  @-}
pCoef :: (Num a) => Poly a -> Int -> a
pCoef p@(P d ps) i = vGet ps i

-- | Get the coefficient of a polynomial as a polynomial.
{-@ pLeadCoef :: Poly a -> Poly a @-}
pLeadCoef p@(P d cs) = P 0 $ V 1 [vHd cs]

-- | Add two polynomials.
--
-- >>> p = P 2 $ V 3 [1,1,1]
-- >>> p
-- X^2 + X + 1
-- >>> q = P 3 $ V 4 [1,0,0,1]
-- >>> q
-- X^3 + 1
-- >>> p + q
-- X^3 + X^2 + X + 2
{-@ addPoly :: (Eq a, Num a)
            => p:Poly a
            -> q:Poly a
            -> Poly a
  @-}
addPoly :: (Eq a, Num a) => Poly a -> Poly a -> Poly a
addPoly p q = zipPolyWith (+) p q

-- | Subtract two polynomials.
--
-- >>> p = P 2 $ V 3 [1,1,1]
-- >>> p
-- X^2 + X + 1
-- >>> q = P 3 $ V 4 [1,0,0,1]
-- >>> q
-- X^3 + 1
-- >>> p - q
-- -1*X^3 + X^2 + X
{-@ subPoly :: (Eq a, Num a)
            => p:Poly a
            -> q:Poly a
            -> Poly a 
  @-}
subPoly :: (Eq a, Num a) => Poly a -> Poly a -> Poly a
subPoly p q = zipPolyWith (-) p q

-- | Multiply two polynomials.
--
-- >>> p = P 2 $ V 3 [1,1,1]
-- >>> p
-- X^2 + X + 1
-- >>> q = P 3 $ V 4 [1,0,0,1]
-- >>> q
-- X^3 + 1
-- >>> p * q
-- X^5 + X^4 + X^3 + X^2 + X + 1
{-@ multPoly :: (Eq a, Num a)
             => p:Poly a
             -> q:Poly a
             -> Poly a
  @-}
multPoly :: (Eq a, Num a) => Poly a -> Poly a -> Poly a
multPoly p@(P _ ps) q@(P _ qs) = pLoop $ pd - 1
  where
    pd = vDim ps
    qd = vDim qs

    pLoop i = let r = qLoop i $ qd - 1
              in
                if i > 0
                then r + (pLoop $ i - 1)
                else r

    qLoop i j = let r = multTerms i j
                in
                  if j > 0
                  then r + (qLoop i $ j - 1)
                  else r

    {-@ multTerms :: i:{Nat | i < vDim ps}
                  -> j:{Nat | j < vDim qs}
                  -> r:Poly a @-}
    multTerms i j = P (kd - 1) $ V kd rs
      where
        -- Degree of p/q/r terms indicated by i/j/k.
        di = pd - i - 1
        dj = qd - j - 1
        dk = di + dj
        kd = dk + 1
        -- Coefficients of p/q/r terms indicated by i/j/k.
        ci = vGet ps i
        cj = vGet qs j
        ck = ci * cj
        -- Coefficients of result r, set lead term to product
        -- of coefficients and the rest to zero.
        --
        -- Note that we multiply the coefficient by zero in the
        -- latter case to give coefficient implementation a chance
        -- to set extra info on the number (e.g. GF field polynomial).
        rs = map' (\n -> if n == 1 then ck else ck * 0) $ nseq kd

-- | Divide one polynomial by another and return the
--   quotient and remainder.
--
-- https://en.wikipedia.org/wiki/Polynomial_long_division#Pseudocode
--
-- >>> p = P 2 $ V 3 [1,1,1]
-- >>> p
-- X^2 + X + 1
-- >>> q = P 3 $ V 4 [1,0,0,1]
-- >>> q
-- X^3 + 1
-- >>> p `quotRem` q
-- (,X^2 + X + 1)
{-@ quoRemPoly :: (Integral a)
               => p:Poly a
               -> q:Poly a
               -> (Poly a, Poly a)
  @-}
quoRemPoly :: (Integral a)
           => Poly a
           -> Poly a
           -> (Poly a, Poly a)
quoRemPoly n d = go 0 n
  where
    -- Disable termination checking because we currently have no
    -- way of proving the remainder decreases. Proving this
    -- generically may not even be possible.
    {-@ lazy go @-}
    {-@ go :: q:Poly a -> r:Poly a -> (Poly a, Poly a) @-}
    go q r =
      if r /= 0 && pDeg r >= pDeg d
      then
        let t = divLeadTerms r d
            q' = q + t
            r' = r - t * d
        in go q' r'
      else (q, r)
      
    divLeadTerms p@(P pd ps) q@(P qd qs) = P pr $ V (pr + 1) rs
      where
        -- Degree of r term.
        pr = if pd >= qd then pd - qd else 0
        -- Coefficients of p/q/r terms.
        cp = vGet ps 0
        cq = vGet qs 0
        -- This is messy but avoid DBZ by setting quotient to zero.
        -- Could we instead assert that denominator is non-zero?
        -- LH does not seem to like this with generics.
        cr = if cq == 0 then 0 else cp `quot` cq
        -- Coefficients of result r, set lead term to quotient
        -- and rest to zero.
        --
        -- Note that we multiply the coefficient by zero in the
        -- latter case to give coefficient implementation a chance
        -- to set extra info on the number (e.g. GF field polynomial).
        rs = map' (\n -> if n == 1 then cr else cr * 0) $ nseq (pr + 1)

-- | Evaluate a polynomial by plugging in a value for X.
evalPoly p@(P d (V n cs)) x = sum $ zipWith (\c i -> c * x^i) cs powers
  where
    powers = map' (+(-1)) $ reverse' $ nseq n

-- | Get the inverse of a polynomial mod another polynomial. This is EEA plus an extra
--   step of dividing by the leading coefficient of one of the outputs of the EEA. The output
--   of this matches the XGCD function from Sage/NTL.
-- 
-- http://juaninf.blogspot.com/2013/04/function-make-div-with-id-mycell-sage.html
-- Algorithm 3.2.2 of Cohen, GTM 138
-- https://ebookcentral.proquest.com/lib/rit/reader.action?docID=3098147
{-@ polyInvMod :: a:_ -> g:{_ | g /= 0} -> _ @-}
polyInvMod :: (Integral a) => Poly a -> Poly a -> Poly a
polyInvMod a g =
  let (g', u, _) = extEuclidAlg a g
      g'' = pLeadCoef g'
  in
    -- Avoid DBZ by setting quotient to zero.
    -- Could we prove this won't happen somehow?
    if g'' == 0 then 0 else u `quot` g''
