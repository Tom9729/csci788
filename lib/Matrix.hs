{- |
Description : CSCI-788 Capstone Project - Matrix math.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

This module is based on the following LiquidHaskell tutorial:
https://ucsd-progsys.github.io/liquidhaskell-tutorial/Tutorial_07_Measure_Int.html;
specifically the types and matProduct and transpose; the rest is
my own work except where noted.
-}
module Matrix where

import Base
import List
import Vector
import Data.List (intercalate)

{-@ data Matrix a =
           M { mRow :: Pos
             , mCol :: Pos
             , mElts :: VectorN (VectorN a mCol) mRow
             }
  @-}
{-@ type MatrixS a = {v:Matrix a | mRow v = mCol v} @-}
{-@ type MatrixN a R C = {v:Matrix a | Dims v R C} @-}
{-@ type MatrixSN a N = {v:Matrix a | Dims v N N} @-}
{-@ type MatrixX a X = MatrixN a {mRow X} {mCol X} @-}
{-@ predicate Dims M R C = mRow M = R && mCol M = C @-}
data Matrix a = M
  {
    mRow :: Int
  , mCol :: Int
  , mElts :: Vector (Vector a)
  } deriving (Eq)

-- | Pretty-print matrices.
-- Idea from Data.Matrix.prettyMatrix.
-- https://hackage.haskell.org/package/matrix-0.3.6.1/docs/src/Data-Matrix.html
-- TODO smartly space out elements like Data.Matrix does
instance (Num a, Show a) => (Show (Matrix a)) where
  show (M r c (V _ rows)) = intercalate "\n" $
    map (\(V _ columns) ->
           "| " ++ (unwords $ map (show) columns) ++ " |"
        )
    rows

-- | Product of two matrices.
--
-- Examples:
--
-- >>> a = M 3 2 $ V 3 [V 2 [1,2], V 2 [3,4], V 2 [5,6]]
-- >>> b = M 2 1 $ V 2 [V 1 [1], V 1 [3]]
-- >>> a
-- | 1 2 |
-- | 3 4 |
-- | 5 6 |
-- >>> b
-- | 1 |
-- | 3 |
-- >>> a `matProduct` b
-- | 7 |
-- | 15 |
-- | 23 |
{-@ matProduct :: (Num a) => x:Matrix a ->
                             y:{Matrix a | mCol x = mRow y} ->
                             MatrixN a (mRow x) (mCol y)
  @-}
matProduct (M rx _ xs) my@(M _ cy _) = M rx cy elts
  where
    elts = for xs $ \xi ->
                      for ys' $ \yj ->
                                  dotProduct xi yj
    M _ _ ys' = transpose my

-- | Add two matrices.
--
-- Examples:
--
-- >>> a = M 2 1 $ V 2 [V 1 [3], V 1 [4]]
-- >>> b = M 2 1 $ V 2 [V 1 [7], V 1 [2]]
-- >>> a
-- | 3 |
-- | 4 |
-- >>> b
-- | 7 |
-- | 2 |
-- >>> matAdd a b
-- | 10 |
-- | 6 |
{-@ matAdd :: (Num a) => x:Matrix a ->
                         y:MatrixX a x ->
                         MatrixX a x @-}
matAdd (M rx cx xs) (M _ _ ys) = M rx cx $
  vZipWith (\a b -> vZipWith (+) a b) xs ys

-- | Swap rows and columns.
--
-- Examples:
--
-- >>> m = M 3 2 $ V 3 [V 2 [1,2], V 2 [3,4], V 2 [5,6]]
-- >>> m
-- | 1 2 |
-- | 3 4 |
-- | 5 6 |
-- >>> transpose m
-- | 1 3 5 |
-- | 2 4 6 |
{-@ transpose :: x:Matrix a -> MatrixN a (mCol x) (mRow x) @-}
transpose (M r c rows) = M c r $ txgo c r rows
  where
    {-@ txgo :: c:Nat ->
                r:Pos ->
                VectorN (VectorN a c) r ->
                VectorN (VectorN a r) c / [r]
      @-}
    txgo :: Int ->
            Int ->
            Vector (Vector a) ->
            Vector (Vector a)
    txgo c 1 (V _ (row:[])) = for row $ \x -> V 1 [x]
    txgo c r (V _ (row:rows)) = vZipWith (vCons) row $
                                txgo c (r - 1) $ V (r - 1) rows

-- | Convert vector to a row matrix.
--
-- Examples:
--
-- >>> matFromVec $ V 3 [1,2,3]
-- | 1 2 3 |
{-@ matFromVec :: v:VectorNE a -> MatrixN a 1 {vDim v} @-}
matFromVec :: Vector a -> Matrix a
matFromVec (V n xs) = M 1 n $ V 1 [V n xs]

-- | Convert row matrix to a vector.
--
-- Examples:
--
-- >>> matToVec $ M 1 3 $ V 1 [V 3 [1,2,3]]
-- | 1 2 3 |
{-@ matToVec :: m:{Matrix a | mRow m = 1} -> VectorN a {mCol m} @-}
matToVec m = vHd $ mElts m

-- | Get identity matrix of size N.
--
-- Examples:
--
-- >>> identity 3
-- | 1 0 0 |
-- | 0 1 0 |
-- | 0 0 1 |
{-@ identity :: (Num a) => x:Pos -> MatrixSN a x @-}
identity :: (Num a) => Int -> Matrix a
identity n = M n n $ rows n
  where 
    rows n = V n $ map' (\i -> cols n i) $ nseq n
    cols n i = V n $ map' (\j -> if j == i then 1 else 0) $ nseq n

-- | Swap two rows of a matrix.
--
-- Examples:
--
-- >>> swapRows (identity 3) 0 2
-- | 0 0 1 |
-- | 0 1 0 |
-- | 1 0 0 |
{-@ swapRows :: m:Matrix a ->
                i:{Nat | i < mRow m} ->
                j:{Nat | j < mRow m}  ->
                MatrixX a m @-}
swapRows :: Matrix a -> Int -> Int -> Matrix a
swapRows m@(M r c rows) i j = M r c $ vSwap rows i j 

-- | Add one row `i` of a matrix to row `j`.
--
-- Examples:
--
-- >>> addRows (identity 3) 0 2
-- | 1 0 0 |
-- | 0 1 0 |
-- | 1 0 1 |
{-@ addRows :: (Num a) => m:Matrix a ->
               i:{Nat | i < mRow m} ->
               j:{Nat | j < mRow m}  ->
               MatrixX a m @-}
addRows :: (Num a) => Matrix a -> Int -> Int -> Matrix a
addRows m@(M r c rows) i j =
  M r c $ forN rows $ \(row, n) ->
                        if (n - 1) == j
                        then vZipWith (+) row $ vGet rows i
                        else row

-- | Find index `i` of some row in matrix `m` such that predicate `f` evaluates
-- | to true; the first `n` rows are skipped.
--
-- Examples:
--
-- >>> m = M 3 3 $ V 3 [V 3 [1,0,0], V 3 [1,1,0], V 3 [0,1,1]]
-- >>> findRowN m (\row -> 1 == vGet row 0) 1
-- Just 1
{-@ findRowN :: m:Matrix a ->
                (VectorN a (mCol m) -> Bool) ->
                n:{Nat | n < mRow m} ->
                i:{Maybe Nat | MaybeLT i (mRow m)} @-}
findRowN :: Matrix a ->
            (Vector a -> Bool)
            -> Int
            -> Maybe Int
findRowN m@(M r c rows) f n = vFindN rows f n

-- | Drop the first `n` columns from a matrix `m`.
--
-- Examples:
--
-- >>> m = M 3 3 $ V 3 [V 3 [1,0,0], V 3 [1,1,0], V 3 [0,1,1]]
-- >>> dropColumns m 2
-- | 0 |
-- | 0 |
-- | 1 |
{-@ dropColumns :: m:Matrix a ->
                   n:{Nat | n < mCol m} ->
                   m':{Matrix a | mRow m' = mRow m && mCol m' = mCol m - n} @-}
dropColumns :: Matrix a -> Int -> Matrix a
dropColumns m@(M r c rows) n =
  M r (c - n) $ for rows $ \row -> vDrop row n

-- | Take the first `n` columns from a matrix `m`.
--
-- Examples:
--
-- >>> m = M 3 3 $ V 3 [V 3 [1,0,0], V 3 [1,1,0], V 3 [0,1,1]]
-- >>> takeColumns m 2
-- | 1 0 |
-- | 1 1 |
-- | 0 1 |
{-@ takeColumns :: m:Matrix a ->
                   n:{Pos | n < mCol m} ->
                   m':MatrixN a {mRow m} n @-}
takeColumns :: Matrix a -> Int -> Matrix a
takeColumns m@(M r c rows) n =
  M r n $ for rows $ \row -> vTake row n

-- | Append matrix `m` to matrix `n` by concatenating the rows.
--
-- Examples:
--
-- >>> m = M 3 3 $ V 3 [V 3 [1,0,0], V 3 [1,1,0], V 3 [0,1,1]]
-- >>> n = identity 3
-- >>> matAppend m n
-- | 1 0 0 1 0 0 |
-- | 1 1 0 0 1 0 |
-- | 0 1 1 0 0 1 |
{-@ matAppend :: m:Matrix a ->
                 n:{Matrix a | mRow m = mRow n} ->
                 o:{Matrix a | mRow o = mRow m && mCol o = mCol m + mCol n} @-}
matAppend :: Matrix a -> Matrix a -> Matrix a
matAppend m@(M mr mc mrows) n@(M _ nc nrows) =
  M mr (mc + nc) $ vZipWith (\mrow nrow ->
                               vConcat mrow nrow) mrows nrows
