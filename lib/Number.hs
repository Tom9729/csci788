{- |
Description : CSCI-788 Capstone Project - Number theory functions.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

Note: With Z3 4.8.5 verification of this module would sometimes get stuck.
      Upgrading to 4.8.15 resolved this issue.
-}
module Number where

-- | Extended Euclidean Algorithm from "Cryptograph: Theory
-- and Practice", 4th edition section 6.2.
--
-- Converted to use recursion instead of a while-loop.
--
-- >>> extEuclidAlg 75 28
-- (1,3,-8)
--
-- >>> extEuclidAlg 100 10
-- (10,0,1)
{-@ extEuclidAlg :: (Integral a) =>
                 a:a
                 -> b:{a | b /= 0}
                 -> (a, a, a)
  @-}
extEuclidAlg :: (Integral a) => a -> a -> (a, a, a)
extEuclidAlg a b =
  let (q, r) = a `quotRem` b
  in
    go 0 1 1 0 a b q r
    
  where
    {-@ go :: (Integral a)
           => t0:a
           -> t:a
           -> s0:a
           -> s:a
           -> a0:a
           -> b0:a
           -> q:a
           -> r:{a | b0 > r}
           -> (a,a,a) / [r] @-}
    go :: (Integral a) => a -> a -> a -> a -> a -> a -> a -> a -> (a,a,a)
    go t0 t s0 s a0 b0 q r =
      if r == 0 then (b0, s, t)
      else
        let t' = t0 - q * t
            s' = s0 - q * s
            (q',r') = b0 `quotRem` r
        in
          go t t' s s' b0 r q' r'
