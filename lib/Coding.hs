{- |
Description : CSCI-788 Capstone Project - Coding theory functions.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>
-}
module Coding where

import Bit
import Matrix
import List
import Vector

-- | Find an error vector that matches our syndrome.
-- This can be used to correct the error in the received message.
{-@ findError :: h:Matrix Bit ->
                 s:{Matrix Bit | mCol s == 1 && mRow s = mRow h} ->
                 Maybe (MatrixN Bit 1 (mCol h))
  @-}
findError :: Matrix Bit -> Matrix Bit -> Maybe (Matrix Bit)
findError h@(M _ c _) s = go c c
  where
    -- Get 1 in Nth position like [1,2,3,4,...] -> [0,0,1,0,0,...]
    {-@ errorN :: n:Nat -> c:Nat -> ListN Bit c @-}
    errorN :: Int -> Int -> List Bit
    errorN n c = map' (\i -> if i == n then 1 else 0) $ nseq c

    -- Get row matrix for the error.
    {-@ errorM :: n:Pos -> c:Pos -> MatrixN Bit 1 c @-}
    errorM :: Int -> Int -> Matrix Bit
    errorM n c = matFromVec $ V c $ errorN n c

    -- Search for errors that produce the desired syndrome.
    {-@ go :: n:Nat -> c:{Pos | c = mCol h} -> Maybe (MatrixN Bit 1 c) @-}
    go :: Int -> Int -> Maybe (Matrix Bit)
    go 0 c = Nothing
    go n c =
      let errorMat = errorM n c
          errorMat' = transpose errorMat
      in
        if s == h `matProduct` errorMat'
        then Just errorMat
        else go (n - 1) c
