{- |
Description : CSCI-788 Capstone Project - List operations.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

This module is based on the following LiquidHaskell tutorial:
https://ucsd-progsys.github.io/liquidhaskell-tutorial/Tutorial_07_Measure_Int.html;
specifically the basic functions and types, nseq and down is my own work.
-}
{-# LANGUAGE StrictData, ScopedTypeVariables #-}
module List where

import Base
import Data.Maybe

{-@ measure size @-}
{-@ size :: [a] -> Nat @-}
size :: [a] -> Int
size [] = 0
size (_:xs) = 1 + size xs

{-@ measure notEmpty @-}
notEmpty :: [a] -> Bool
notEmpty [] = False
notEmpty _ = True

{-@ type ListN a N = {v:List a | size v = N} @-}
{-@ type ListX a X = ListN a {size X} @-}
{-@ type ListNE a = {v:List a | notEmpty v} @-}
{-@ type ListGE a N = {v:List a | size v >= N} @-}
{-@ type ListLE a N = {v:List a | size v <= N} @-}
type List a = [a]

-- | Apply a function to items in a list.
--
-- Examples:
--
-- >>> map' (+1) [1,2,3]
-- [2,3,4]
{-@ map' :: (a -> b) -> xs:List a -> ListX b xs @-}
map' _ [] = []
map' f (x:xs) = f x : map' f xs

-- | Add all of the items in a list.
--
-- Examples:
--
-- >>> sum' [1,2,3]
-- 6
--
-- TODO revisit making this generic
{-@ sum' :: List Int -> Int @-}
sum' :: List Int -> Int
sum' [] = 0
sum' (x:[]) = x
sum' (x:xs) = x + (sum' xs)

-- | Combine pairs of elements from two lists with a function.
--
-- Examples:
--
-- >>> zipWith' (,) [1,2,3] [4,5,6]
-- [(1,4),(2,5),(3,6)]
{-@ zipWith' :: (a -> b -> c) ->
               xs:List a ->
               ys:ListX b xs ->
               ListX c xs
  @-}
zipWith' _ [] [] = []
zipWith' f (x:xs) (y:ys) =
  f x y : zipWith' f xs ys

-- | Make a list of a certain size.
--
-- Normally would use generator for this but LH doesn't
-- seem to understand that.
--
-- Examples:
--
-- >>> nseq 4
-- [1,2,3,4]
{-@ nseq :: n:Nat -> ListN Pos n @-}
nseq :: Int -> List Int
nseq n = go n 1
  where
    go :: Int -> Int -> List Int
    go 0 _ = []
    go n i = i : go (n - 1) (i + 1)

-- | Take first N values from a list.
--
-- Examples:
--
-- >>> take 2 [1,2,3]
-- [1,2]
{-@ take' :: n:Nat -> xs:ListGE a n -> ListN a n @-}
take' :: Int -> List a -> List a
take' 0 _ = []
take' n (x:xs) = x : (take' (n - 1) xs)

-- | Drop n-items from a list.
--
-- Examples:
--
-- >>> drop' 2 [1,2,3]
-- [3]
{-@ drop' :: n:Nat ->
             xs:{List a | n <= size xs} ->
             ys:{List a | size ys = size xs - n} @-}
drop' :: Int -> List a -> List a
drop' 0 xs = xs
drop' n (_:xs) = drop' (n - 1) xs

-- | Drop items from a list while a predicate evaluates to true.
--
-- Examples:
--
-- >>> dropWhile' (<3) [1,2,3]
-- [3]
{-@ dropWhile' :: (a -> Bool)
               -> xs:List a
               -> ys:{List a | size ys <= size xs} @-}
dropWhile' :: (a -> Bool) -> List a -> List a
dropWhile' f [] = []
dropWhile' f l@(x:xs) =
  if f x
  then dropWhile' f xs
  else l

-- | Find the index of the first item in a list that matches a predicate.
--
-- Examples:
--
-- >>> find [1,2,3] (>1) 
-- Just 1
{-@ find :: xs:List a ->
            (a -> Bool) ->
            i:{Maybe Nat | MaybeLT i (size xs)} @-}
find xs f = if dlen == 0
            then Nothing
            else Just $ xlen - dlen
  where
    xlen = size xs
    dlen = size $ dropWhile' (not.f) xs

-- | Find the index of the first item in a list that matches a predicate
--   after skipping the first n-elements.
--
-- Examples:
--
-- >>> findN [1,2,3] (>1) 2
-- Just 2
{-@ findN :: xs:List a ->
             (a -> Bool) ->
             n:{Nat | n < size xs} ->
             i:{Maybe Nat | MaybeLT i (size xs)} @-}
findN :: List a -> (a -> Bool) -> Int -> Maybe Int
findN xs f n = case find xs' f of
                 Just i -> Just $ n + i
                 _ -> Nothing
  where
    xs' = drop' n xs

-- | Get an item from a list by index.
--
-- Examples:
--
-- >>> get [1,2,3] 2
-- 3
{-@ get :: xs:List a ->
           i:{Nat | i < size xs} ->
           a @-}
get :: List a -> Int -> a
get (x:xs) i =
  if i == 0
  then x
  else get xs $ i - 1

-- | Reverse a list.
--
-- Examples:
--
-- >>> reverse' [1,2,3]
-- [3,2,1]
{-@ reverse' :: xs:List a -> ListX a xs @-}
reverse' :: List a -> List a
reverse' xs = go xs []
  where
    {-@ go :: xs:List a -> ys:List a -> ListN a {size xs + size ys} @-}
    go :: List a -> List a -> List a
    go [] ys = ys
    go (x:xs) ys = go xs $ x:ys

-- | Concatenate two lists.
--
-- Examples:
--
-- >>> concat' [1,2,3] [4,5,6]
-- [1,2,3,4,5,6]
{-@ concat' :: xs:List a ->
               ys:List a ->
               zs:ListN a {size xs + size ys} @-}
concat' :: List a -> List a -> List a
concat' xs ys = go (reverse' xs) ys
  where
    {-@ go :: xs:List a -> ys:List a -> ListN a {size xs + size ys} @-}
    go :: List a -> List a -> List a
    go [] ys = ys
    go (x:xs) ys = go xs $ x:ys

-- | Split a list into two lists of alternating values.
--
-- Examples:
--
-- >>> altList [1,2,3,4,5]
-- ([1,3,5],[2,4])
{-@ altList :: xs:List a -> (List a, List a) @-}
altList xs = go True xs
  where
    go alt [] = ([], [])
    go alt (x:[]) = if alt
                    then ([x], [])
                    else ([], [x])
    go alt (x:y:[]) = if alt
                      then ([x], [y])
                      else ([y], [x])
    go alt (x:xs) =
      let (left, right) = go (not alt) xs
      in
        if alt
        then (x:left, right)
        else (left, x:right)

-- | Split a list into partitions of a given size.
--
-- Examples:
--
-- >>> partitionList [1,2,3,4,5] 2
-- [[1,2,3],[4,5]]
-- >>> partitionList [1,2,3,4,5] 3
-- [[1,2],[3,4],[5]]
{-@ partitionList :: xs:List a -> n:Pos -> ListN (List a) n @-}
partitionList :: List a -> Int -> List (List a)
partitionList xs n = go n xs
  where
    l = let (q,r) = size xs `quotRem` n
        in if q > r then q + r
           else r

    {-@ go :: i:Nat -> xs:List a -> ListN (List a) i / [i] @-}
    go :: Int -> List a -> List (List a)
    go 0 xs = []
    go i xs =
      let (left, right) = split xs [] []
      in left : (go (i - 1) right)

    split [] left right = (reverse' left, reverse' right)
    split (x:xs) left right =
      if size left < l
      then split xs (x:left) right
      else split xs left (x:right)
