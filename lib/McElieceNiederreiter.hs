{- |
Description : CSCI-788 Capstone Project - McEliece Niederreiter toy implementation.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

Toy implementation of Niederreiter variant of McEliece using Patterson's
algorithm to decode the ciphertext. This is based on work by Risse,
Roering, Minihold, and Hu.

https://en.wikipedia.org/wiki/Niederreiter_cryptosystem
http://www.ubicc.org/files/pdf/SAGE_Goppa_McEliece_595.pdf
https://digitalcommons.csbsju.edu/cgi/viewcontent.cgi?article=1019&context=honors_theses
https://images.derstandard.at/2013/10/04/Matthias-Minihold.pdf
https://github.com/davidhoo1988/Code_Based_Cryptography_Python/blob/master/src/GoppaCode.sage
-}
module McElieceNiederreiter where

import Vector
import Bit
import Polynomial
import GaloisField
import Goppa
import Number
import Matrix
import BitMatrix
import Data.Maybe
import List

-- | (Private) Galois field irreducible polynomial.
--
-- >>> f
-- (z^3 + z^2 + 1)
{-@ assume f :: f:{GF2Poly | f /= 0} @-}
f :: GF2Poly
f = P 3 $ V 4 [1, 1, 0, 1]

-- | (Private) Irreducible Goppa polynomial [X^2 + X + 1].
--
-- >>> g
-- X^2 + X + (1)
{-@ assume g :: g:{Poly GF2Element | g /= 0 && pDeg g = 2} @-}
g :: Poly GF2Element
g = P 2 $ V 3
  [
    E f $ P 0 $ V 1 [1]
  , E f $ P 0 $ V 1 [1]
  , E f $ P 0 $ V 1 [1]
  ]

-- | (Private) Polynomial Z (used for code locators).
--
-- >>> z
-- (z)
z :: GF2Element
z = E f $ P 1 $ V 2 [1, 0]

-- | (Private) Polynomial X (used for error locator).
--
-- >>> x
-- X
x :: Poly GF2Element
x = P 1 $ V 2
  [
    E f $ P 0 $ V 1 [1]
  , 0
  ]

-- | (Private) Generate code locators AKA supports.
--
-- >>> codeLocators
-- [(),(1),(z^2),(z^2 + z + 1),(z^2 + z),(z),(z^2 + 1),(z + 1)]
{-@ codeLocators :: ListN GF2Element 8 @-}
codeLocators :: List GF2Element
codeLocators = (p*0):(1+p*0):(map' (\i -> p^i) $ nseq 6)
  where
    -- Primitive root.
    -- TODO calculate this
    p = z^2

-- | (Private) Goppa code parity matrix H.
-- TODO calculate this instead of hardcoding
--
-- >>> parity
-- | 1 0 0 1 0 0 0 1 |
-- | 0 0 0 1 0 1 1 1 |
-- | 0 0 1 1 1 0 0 1 |
-- | 1 1 0 1 1 1 0 1 |
-- | 0 0 1 1 1 0 1 0 |
-- | 0 0 1 0 0 1 1 1 |
{-@ parity :: MatrixN Bit 6 8 @-}
parity :: Matrix Bit
parity = M 6 8 $ V 6
  [
    V 8 [1, 0, 0, 1, 0, 0, 0, 1]
  , V 8 [0, 0, 0, 1, 0, 1, 1, 1]
  , V 8 [0, 0, 1, 1, 1, 0, 0, 1]
  , V 8 [1, 1, 0, 1, 1, 1, 0, 1]
  , V 8 [0, 0, 1, 1, 1, 0, 1, 0]
  , V 8 [0, 0, 1, 0, 0, 1, 1, 1]
  ]

-- | (Private) Scrambler matrix S.
--
-- >>> scrambler
-- | 1 1 0 1 1 0 |
-- | 1 0 1 1 1 1 |
-- | 0 0 1 0 1 1 |
-- | 0 0 1 1 0 0 |
-- | 1 1 0 0 0 0 |
-- | 1 1 1 0 0 0 |
{-@ scrambler :: MatrixN Bit 6 6 @-}
scrambler :: Matrix Bit
scrambler = M 6 6 $ V 6
  [
    V 6 [1, 1, 0, 1, 1, 0]
  , V 6 [1, 0, 1, 1, 1, 1]
  , V 6 [0, 0, 1, 0, 1, 1]
  , V 6 [0, 0, 1, 1, 0, 0]
  , V 6 [1, 1, 0, 0, 0, 0]
  , V 6 [1, 1, 1, 0, 0, 0]
  ]

-- | (Private) Scrambler matrix inverse S^-1.
--
-- >>> scramblerInv
-- | 0 1 1 1 1 1 |
-- | 0 1 1 1 0 1 |
-- | 0 0 0 0 1 1 |
-- | 0 0 0 1 1 1 |
-- | 1 0 0 1 0 1 |
-- | 1 0 1 1 1 0 |
{-@ scramblerInv :: MatrixN Bit 6 6 @-}
scramblerInv :: Matrix Bit
scramblerInv = case bitMatInv scrambler of
  Just m -> m
  -- Default to identity if no inverse found.
  -- We should signal an error condition here instead.
  _ -> identity 6

-- | (Private) Permutation matrix P.
--
-- >>> permutation
-- | 0 0 0 0 0 0 0 1 |
-- | 0 0 0 0 0 0 1 0 |
-- | 0 0 0 0 0 1 0 0 |
-- | 0 1 0 0 0 0 0 0 |
-- | 0 0 0 1 0 0 0 0 |
-- | 0 0 1 0 0 0 0 0 |
-- | 0 0 0 0 1 0 0 0 |
-- | 1 0 0 0 0 0 0 0 |
{-@ permutation :: MatrixN Bit 8 8 @-}
permutation :: Matrix Bit
permutation = M 8 8 $ V 8
  [
    V 8 [0, 0, 0, 0, 0, 0, 0, 1]
  , V 8 [0, 0, 0, 0, 0, 0, 1, 0]
  , V 8 [0, 0, 0, 0, 0, 1, 0, 0]
  , V 8 [0, 1, 0, 0, 0, 0, 0, 0]
  , V 8 [0, 0, 0, 1, 0, 0, 0, 0]
  , V 8 [0, 0, 1, 0, 0, 0, 0, 0]
  , V 8 [0, 0, 0, 0, 1, 0, 0, 0]
  , V 8 [1, 0, 0, 0, 0, 0, 0, 0]
  ]

-- | (Private) Permutation matrix inverse P^-1.
--
-- >>> permutationInv
-- | 0 0 0 0 0 0 0 1 |
-- | 0 0 0 1 0 0 0 0 |
-- | 0 0 0 0 0 1 0 0 |
-- | 0 0 0 0 1 0 0 0 |
-- | 0 0 0 0 0 0 1 0 |
-- | 0 0 1 0 0 0 0 0 |
-- | 0 1 0 0 0 0 0 0 |
-- | 1 0 0 0 0 0 0 0 |
{-@ permutationInv :: MatrixN Bit 8 8 @-}
permutationInv :: Matrix Bit
permutationInv = case bitMatInv permutation of
  Just m -> m
  -- Default to identity if no error found.
  -- We should signal an error condition here instead.
  _ -> identity 8

-- | (Public) Public parity matrix H'.
--
-- >>> parity'
-- | 1 0 0 0 0 1 1 0 |
-- | 0 0 0 1 0 1 1 0 |
-- | 0 0 1 0 0 1 0 0 |
-- | 0 0 1 0 0 1 1 1 |
-- | 0 0 1 0 1 0 0 1 |
-- | 1 1 1 1 1 1 0 1 |
{-@ parity' :: MatrixN Bit 6 8 @-}
parity' :: Matrix Bit
parity' = (scrambler `matProduct` parity) `matProduct` permutation

-- | Alice encodes message as error vector E.
--
-- >>> ptext
-- | 0 0 1 0 0 0 1 0 |
{-@ ptext :: VectorN Bit 8 @-}
ptext :: Vector Bit
ptext = V 8 [0, 0, 1, 0, 0, 0, 1, 0]

-- | Alice produces cipher text Y.
--
-- >>> ctext
-- | 1 1 1 0 1 1 |
{-@ ctext :: MatrixN Bit 1 6 @-}
ctext :: Matrix Bit
ctext = transpose $ parity' `matProduct` (transpose $ matFromVec ptext)

-- | Bob unscrambles the ciphertext.
--
-- >>> ctext'
-- | 0 1 0 0 0 1 |
{-@ ctext' :: MatrixN Bit 1 6 @-}
ctext' = transpose $ scramblerInv `matProduct` (transpose ctext)

-- | Bob converts the ciphertext/syndrome to a polynomial.
--
-- >>> syndrome
-- (z^2)*X + (z)
syndrome :: Poly GF2Element
syndrome = goppaSyndromePoly (matToVec ctext') f g

-- | Bob calculates the error locator polynomial using Patterson's
--   algorithm.
--
-- >>> errorLocator
-- (z^2)*X^2 + X + (z^2 + 1)
errorLocator = pattersonsAlgorithm x g syndrome

-- | Determine error using ELP.
--
-- >>> ctextError
-- | 0 1 0 0 0 1 0 0 |
{-@ ctextError :: VectorN Bit 8 @-}
ctextError :: Vector Bit
ctextError = V 8 $ map' (\c -> if 0 == evalPoly errorLocator c then 1 else 0) codeLocators

-- | Remove permutation getting original message/error.
--
-- >>> ptext'
-- | 0 0 1 0 0 0 1 0 |
ptext' = matToVec $ transpose $ permutationInv `matProduct` (transpose $ matFromVec ctextError)
