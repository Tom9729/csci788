{- |
Description : CSCI-788 Capstone Project - McEliece Goppa Code toy implementation.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

Note: Computing the generator matrix from the Goppa polynomial still needs to be done;
the current example is from a book (referenced below) and reproduced in Sage.
Also we have not verified termination for all functions, for example
the GF2 reduce function is not guaranteed to terminate.

McEliece algorithm described here:
http://www-math.ucdenver.edu/~wcherowi/courses/m5410/ctcmcel.html

Goppa code encoding/decoding from several sources. The current "toy" example code
is from chapter 12 of The Theory Of Error Correcting Codes by Macwilliams
and Sloane. The decoding process is from How Sage Helps To Implement Goppa
Codes And The McEliece Public Key Crypto System by Risse with specifics
from Coding Theory-Based Cryptography: McEliece Cryptosystems In Sage by Roering.
The observation that EEA can be used to decode Goppa codes (with some caveats) is
due to David J.W. HU. The observation that finding polynomial inverse can be done
with EEA with an extra operation at the end is due to Juan Grados and Henri Cohen
(see Chapter 3 of Computational Algebraic Number Theory by Henri Cohen).

Note: Initially we used EEA to decode the ciphertext but this was
changed to Patterson's algorithm during the implementation of
Niederreiter; see McElieceNiederreiter.hs for more details.

https://ebookcentral.proquest.com/lib/rit/reader.action?docID=648815
http://www.ubicc.org/files/pdf/SAGE_Goppa_McEliece_595.pdf
https://digitalcommons.csbsju.edu/cgi/viewcontent.cgi?article=1019&context=honors_theses
https://github.com/davidhoo1988/Code_Based_Cryptography_Python
http://juaninf.blogspot.com/2013/04/function-make-div-with-id-mycell-sage.html
https://ebookcentral.proquest.com/lib/rit/reader.action?docID=3098147

TODO refactor into a library
TODO extend to same message length as hamming example
-}
module McElieceGoppa where

import Vector
import Bit
import Polynomial
import GaloisField
import Goppa
import Number
import Matrix
import BitMatrix
import Data.Maybe
import List

-- | (Private) Galois field irreducible polynomial.
--
-- >>> f
-- (z^3 + z + 1)
{-@ assume f :: f:{GF2Poly | f /= 0} @-}
f :: GF2Poly
f = P 3 $ V 4 [1, 0, 1, 1]

-- | (Private) Irreducible Goppa polynomial [X^2 + X + 1].
--
-- >>> g
-- X^2 + X + (1)
{-@ assume g :: g:{Poly GF2Element | g /= 0} @-}
g :: Poly GF2Element
g = P 2 $ V 3
  [
    E f $ P 0 $ V 1 [1]
  , E f $ P 0 $ V 1 [1]
  , E f $ P 0 $ V 1 [1]
  ]

-- | (Private) Polynomial Z (used for code locators).
--
-- >>> z
-- (z)
z :: GF2Element
z = E f $ P 1 $ V 2 [1, 0]

-- | (Private) Polynomial X (used for syndrome locators).
--
-- >>> x
-- X
x :: Poly GF2Element
x = P 1 $ V 2
  [
    E f $ P 0 $ V 1 [1]
  , 0
  ]

-- | (Private) Generate code locators AKA supports.
--
-- >>> codeLocators
-- [(),(1),(z),(z^2),(z + 1),(z^2 + z),(z^2 + z + 1),(z^2 + 1)]
{-@ codeLocators :: ListN GF2Element 8 @-}
codeLocators :: List GF2Element
codeLocators = (z*0):(1+z*0):(map' (\i -> z^i) $ nseq 6)

-- | (Private) Generate syndrome locators, used to generate
--   syndrome polynomial from ciphertext.
--
-- >>> syndLocators
-- | X + (1) X (z^2)*X + (z^2 + z + 1) (z^2 + z)*X + (z + 1) (z^2)*X + (z + 1) (z)*X + (z^2 + 1) (z)*X + (z^2 + z + 1) (z^2 + z)*X + (z^2 + 1) |
{-@ syndLocators :: VectorN (Poly GF2Element) 8 @-}
syndLocators :: Vector (Poly GF2Element)
syndLocators = V 8 $ [polyInvMod (x - (p c)) g | c <- codeLocators]
  where
    p c = P 0 $ V 1 [c]

-- | (Private) Goppa code generator matrix.
-- TODO calculate this instead of hardcoding
--
-- >>> generator
-- | 1 1 0 0 1 0 1 1 |
-- | 0 0 1 1 1 1 1 1 |
{-@ generator :: MatrixN Bit 2 8 @-}
generator :: Matrix Bit
generator = M 2 8 $ V 2
  [
    V 8 [1, 1, 0, 0, 1, 0, 1, 1]
  , V 8 [0, 0, 1, 1, 1, 1, 1, 1]
  ]

-- | (Private) Find right-inverse of the generator matrix.
-- https://math.stackexchange.com/a/1335707
--
-- >>> generatorInv
-- | 0 1 |
-- | 0 1 |
-- | 1 1 |
-- | 1 1 |
-- | 1 0 |
-- | 1 1 |
-- | 1 0 |
-- | 1 0 |
{-@ generatorInv :: MatrixN Bit 8 2 @-}
generatorInv = matProduct (transpose generator) inv
  where
    maybeInv = bitMatInv $ generator `matProduct` (transpose generator)
    {-@ inv :: MatrixN Bit 2 2 @-}
    inv = case maybeInv of
      Just m -> m
      -- Default to identity if not found.
      -- We should signal an error condition here instead.
      _ -> identity 2

-- | (Private) Scrambler matrix S.
--
-- >>> scrambler
-- | 0 1 |
-- | 1 0 |
{-@ scrambler :: MatrixN Bit 2 2 @-}
scrambler :: Matrix Bit
scrambler = M 2 2 $ V 2
  [
    V 2 [0, 1]
  , V 2 [1, 0]
  ]

-- | (Private) Scrambler matrix inverse S^-1.
--
-- >>> scramblerInv
-- | 0 1 |
-- | 1 0 |
{-@ scramblerInv :: MatrixN Bit 2 2 @-}
scramblerInv :: Matrix Bit
scramblerInv = case bitMatInv scrambler of
  Just m -> m
  -- Default to identity if no inverse found.
  -- We should signal an error condition here instead.
  _ -> identity 2

-- | (Private) Permutation matrix P.
--
-- >>> permutation
-- | 0 1 0 0 0 0 0 0 |
-- | 0 0 0 1 0 0 0 0 |
-- | 0 0 0 0 0 0 1 0 |
-- | 1 0 0 0 0 0 0 0 |
-- | 0 0 1 0 0 0 0 0 |
-- | 0 0 0 0 0 1 0 0 |
-- | 0 0 0 0 1 0 0 0 |
-- | 0 0 0 0 0 0 0 1 |
{-@ permutation :: MatrixN Bit 8 8 @-}
permutation :: Matrix Bit
permutation = M 8 8 $ V 8
  [
    V 8 [0, 1, 0, 0, 0, 0, 0, 0]
  , V 8 [0, 0, 0, 1, 0, 0, 0, 0]
  , V 8 [0, 0, 0, 0, 0, 0, 1, 0]
  , V 8 [1, 0, 0, 0, 0, 0, 0, 0]
  , V 8 [0, 0, 1, 0, 0, 0, 0, 0]
  , V 8 [0, 0, 0, 0, 0, 1, 0, 0]
  , V 8 [0, 0, 0, 0, 1, 0, 0, 0]
  , V 8 [0, 0, 0, 0, 0, 0, 0, 1]
  ]

-- | (Private) Permutation matrix inverse P^-1.
--
-- >>> permutationInv
-- | 0 0 0 1 0 0 0 0 |
-- | 1 0 0 0 0 0 0 0 |
-- | 0 0 0 0 1 0 0 0 |
-- | 0 1 0 0 0 0 0 0 |
-- | 0 0 0 0 0 0 1 0 |
-- | 0 0 0 0 0 1 0 0 |
-- | 0 0 1 0 0 0 0 0 |
-- | 0 0 0 0 0 0 0 1 |
{-@ permutationInv :: MatrixN Bit 8 8 @-}
permutationInv :: Matrix Bit
permutationInv = case bitMatInv permutation of
  Just m -> m
  -- Default to identity if no error found.
  -- We should signal an error condition here instead.
  _ -> identity 8

-- | (Public) Public generator matrix G'.
--
-- >>> generator'
-- | 1 0 1 0 1 1 1 1 |
-- | 0 1 1 1 1 0 0 1 |
{-@ generator' :: MatrixN Bit 2 8 @-}
generator' :: Matrix Bit
generator' = (scrambler `matProduct` generator) `matProduct` permutation

-- | Alice's message X.
--
-- >>> ptext
-- | 1 0 |
{-@ ptext :: VectorN Bit 2 @-}
ptext :: Vector Bit
ptext = V 2 [1, 0]

-- | Error vector E introduced by Alice.
--
-- >>> err
-- | 0 0 0 0 1 0 1 0 |
{-@ err :: VectorN Bit 8 @-}
err :: Vector Bit
err = V 8 [0, 0, 0, 0, 1, 0, 1, 0]

-- | Cipher text Y.
--
-- >>> ctext
-- | 1 0 1 0 0 1 0 1 |
{-@ ctext :: MatrixN Bit 1 8 @-}
ctext :: Matrix Bit
ctext = (ptextM `matProduct` generator') `matAdd` errM
  where
    ptextM = matFromVec ptext
    errM = matFromVec err

-- | Bob calculates Y^-1.
--
-- >>> ctext'
-- | 0 0 0 1 1 1 0 1 |
{-@ ctext' :: MatrixN Bit 1 8 @-}
ctext' = ctext `matProduct` permutationInv

-- | Bob encodes ciphertext as a vector of polynomials.
--   Note that 0 renders as "".
-- >>> ctext''
-- |    (1) (1) (1)  (1) |
{-@ ctext'' :: VectorN (Poly GF2Element) 8 @-}
ctext'' :: Vector (Poly GF2Element)
ctext'' = for (matToVec ctext') (\x -> P 0 $ V 1 [E f $ P 0 $ V 1 [x]])

-- | Bob calculates the syndrome.
--
-- >>> syndrome
-- (z^2 + z)*X
{-@ syndrome :: Poly GF2Element @-}
syndrome :: Poly GF2Element
syndrome = dotProduct syndLocators ctext''

-- | Bob calculates the error locator polynomial using Patterson's
--   algorithm.
--
-- >>> errorLocator
-- (z)*X^2 + X + (1)
errorLocator = pattersonsAlgorithm x g syndrome

-- | Bob calculates the error in the ciphertext by evaluating the error locator
--   polynomial with the codelocators and if the result is zero then the bit
--   in that position indicates an error.
--
-- >>> ctextError
-- | 0 0 1 0 0 0 1 0 |
{-@ ctextError :: VectorN Bit 8 @-}
ctextError :: Vector Bit
ctextError = V 8 $ map' (\c -> if 0 == evalPoly errorLocator c then 1 else 0)
             codeLocators

-- | Bob corrects the error in the ciphertext.
--
-- >>> ctext'''
-- | 0 0 1 1 1 1 1 1 |
{-@ ctext''' :: MatrixN Bit 1 8 @-}
ctext''' = ctext' `matAdd` (matFromVec ctextError)

-- | Bob decodes the ciphertext using the right-inverse
--  of the generator. Basically this strips out the check
--  bits.
--  http://www-math.ucdenver.edu/~wcherowi/courses/m5410/ctcmcel.html
--
-- >>> ctext''''
-- | 0 1 |
{-@ ctext'''' :: MatrixN Bit 1 2 @-}
ctext'''' = ctext''' `matProduct` generatorInv

-- | Bob multiplies by S^-1 to get the original message.
--
-- >>> ptext'
-- | 1 0 |
{-@ ptext' :: MatrixN Bit 1 2 @-}
ptext' = ctext'''' `matProduct` scramblerInv
