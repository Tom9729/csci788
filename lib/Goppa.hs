{- |
Description : CSCI-788 Capstone Project - Goppa code implementation.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

This is partially based on David J.W. Hu's project on Github where he implemented
Goppa Codes & McEliece in SAGE based on the papers by Risse, Roering and others. The
modified EEA is from Matthias Minihold's thesis.

https://github.com/davidhoo1988/Code_Based_Cryptography_Python/blob/master/src/GoppaCode.sage
http://www.ubicc.org/files/pdf/SAGE_Goppa_McEliece_595.pdf
https://digitalcommons.csbsju.edu/cgi/viewcontent.cgi?article=1019&context=honors_theses
https://images.derstandard.at/2013/10/04/Matthias-Minihold.pdf
-}
module Goppa where

import Polynomial
import Vector
import GaloisField
import List
import Bit
import Number

-- | Modified extended Euclid algorithm for Patterson's algorithm.
--
-- https://images.derstandard.at/2013/10/04/Matthias-Minihold.pdf
--
-- Examples:
--
-- >>> f = P 3 $ V 4 [1, 1, 0, 1]
-- >>> g = P 2 $ V 3 [E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1]]
-- >>> r = P 1 $ V 2 [E f $ P 1 $ V 2 [1,0], E f $ P 1 $ V 2 [1,1]]
-- >>> g
-- X^2 + X + (1)
-- >>> r
-- (z)*X + (z + 1)
-- >>> goppaExtEuclidAlg g r
-- (,(z)*X + (z + 1),(1))
goppaExtEuclidAlg a b = go b a 0 1 1 0
  where
    deg = pDeg a
    go 0 lastr _ lastu _ lastv = (lastr, lastu, lastv)
    go r lastr u lastu v lastv =
      let (lastr', (q, r')) = (r, lastr `quotRem` r)
          (u', lastu') = (lastu - q * u, u)
          (v', lastv') = (lastv - q * v, v)
      in
        if pDeg u' <= (deg - 1) `quot` 2 && pDeg r' <= deg `quot` 2
        then (lastu', lastr', lastv')
        else go r' lastr' u' lastu' v' lastv'

-- | Get pair of polynomials (p0, p1) such that p = p0^2 + z * p1^2 (due Risse).
--
-- Examples:
--
-- >>> f = P 3 $ V 4 [1, 1, 0, 1]
-- >>> z = P 1 $ V 2 [E f $ P 0 $ V 1 [1], 0]
-- >>> g = P 2 $ V 3 [E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1]]
-- >>> g
-- X^2 + X + (1)
-- >>> (p0, p1) = goppaSplit g
-- >>> p0
-- X + (1)
-- >>> p1
-- (1)
-- >>> p0^2 + z * p1^2 == g
-- True
{-@ goppaSplit :: p:Poly GF2Element
               -> (Poly GF2Element, Poly GF2Element) @-}
goppaSplit (P d (V n xs)) = (p0, p1)
  where
    p0 = P (nEvens - 1) $ V nEvens evens'
    p1 = P (nOdds - 1) $ V nOdds odds'

    {-@ evens' :: ListGE GF2Element 1 @-}
    (evens, odds) = altList $ reverse' $ map' sqrtGF2 xs
    evens' = if size evens == 0 then [0] else evens
    odds' = if size odds == 0 then [0] else odds
    
    nEvens = size evens'
    nOdds = size odds'

-- | Find the inverse mod the Goppa polynomial for Patterson's algorithm.
--
-- Examples:
--
-- >>> f = P 3 $ V 4 [1, 1, 0, 1]
-- >>> g = P 2 $ V 3 [E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1]]
-- >>> g
-- X^2 + X + (1)
-- >>> g0 = P 1 $ V 2 [E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1]]
-- >>> g0
-- X + (1)
-- >>> g0' = goppaInverse g0 g
-- >>> g0'
-- X
-- >>> g0 * g0' `mod` g == 1
-- True
{-@ goppaInverse :: p:Poly GF2Element
                 -> g:{Poly GF2Element | g /= 0}
                 -> Poly GF2Element
  @-}
goppaInverse :: Poly GF2Element -> Poly GF2Element -> Poly GF2Element
goppaInverse p g = if g == 0
                   then 0
                   -- TODO find way to check this with refinements
                   else u `mod` g
  where
    (d, u, v) = extEuclidAlg p g

-- | Convert a syndrome bit vector to a polynomial.
--
-- Examples:
--
-- >>> f = P 3 $ V 4 [1, 1, 0, 1]
-- >>> g = P 2 $ V 3 [E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1], E f $ P 0 $ V 1 [1]]
-- >>> g
-- X^2 + X + (1)
-- >>> syndrome = V 6 [0, 1, 0, 0, 0, 1]
-- >>> goppaSyndromePoly syndrome f g
-- (z^2)*X + (z)
{-@ goppaSyndromePoly :: s:VectorNE Bit
                      -> f:{Poly Bit | f /= 0}
                      -> g:{Poly _ | pDeg g > 0}
                      -> Poly GF2Element @-}
goppaSyndromePoly s f g = syndPoly
  where
    -- Split bit vector s into this many parts.
    deg = pDeg g

    -- Get list of bits.
    {-@ elts :: ListN Bit {vDim s} @-}
    elts = vElts s

    -- Drop leading zero bits.
    elts' = dropWhile' (==0) elts

    -- Append as many zero bits as we dropped (these two steps
    -- are performing a rotation).
    {-@ elts'' :: ListX Bit elts @-}
    elts'' = concat' elts' $ map' (\_ -> 0) $ nseq $ size elts - size elts'

    -- Split the bits into several lists. Each sub-list will become
    -- a field element coefficient of the syndrome polynomial.
    {-@ parts :: ListN (List Bit) deg @-}
    parts = partitionList elts'' deg

    fElts = map' (\xs ->
                    let xs' = dropWhile' (==0) xs
                        n = size xs'
                    in E f $ if n == 0
                             then P 0 $ V 1 [0]
                             else P (n - 1) $ V n xs') parts

    syndPoly = P (deg - 1) $ V deg fElts

-- | Patterson's algorithm for decoding Goppa codes.
{-@ pattersonsAlgorithm :: x:_ -> g:{_ | g /= 0} -> s:_ -> _ @-}
pattersonsAlgorithm x g syndrome = u^2 + x * v^2
  where
    (g0, g1) = goppaSplit g
    w = g0 * goppaInverse g1 g
    t = polyInvMod syndrome g
    (t0, t1) = goppaSplit $ t + x
    r = t0 + w * t1
    (_, u, v) = goppaExtEuclidAlg g r
