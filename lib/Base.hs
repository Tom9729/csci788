{- |
Description : CSCI-788 Capstone Project - Base type declarations.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>

This module is based on the following LiquidHaskell tutorial:
https://ucsd-progsys.github.io/liquidhaskell-tutorial/Tutorial_07_Measure_Int.html;
specifically the positive int type and the min predicate; the
rest is my own work except where noted.
-}
module Base where

{-@ LIQUID "--bscope" @-}

{-@ type Pos = {v:Int | v > 0} @-}

{-@ predicate MaybeLT I N = (isJust I <=> True && fromJust I < N) ||
                            (isJust I <=> False && I = Nothing) @-}

{-@ impossible :: {v:String | false} -> a  @-}
impossible msg = error msg

{-@ predicate Min X Y Z = (if Y < Z then X = Y else X = Z) @-}
