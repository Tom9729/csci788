{- |
Description : CSCI-788 Capstone Project - Operations on binary matrices.
Copyright : 2022 Tom Arnold <tca4384@rit.edu>
-}
module BitMatrix where

import Bit
import Matrix
import Vector

-- | Calculate inverse of a binary matrix.
{-@ bitMatInv :: x:MatrixS Bit -> Maybe (MatrixX Bit x) @-}
bitMatInv :: Matrix Bit -> Maybe (Matrix Bit)
bitMatInv m =
  if m' == m2
  then Just m2'
  else Nothing
  
  where 
    m2 = identity $ mRow m

    (m', m2') = bitRowReduce m m2

{-@ bitRowReduce :: m:MatrixS Bit ->
                    m2:{Matrix Bit | mRow m = mRow m2} ->
                    (MatrixX Bit m, MatrixX Bit m2) @-}
bitRowReduce :: Matrix Bit ->
                Matrix Bit ->
                (Matrix Bit, Matrix Bit)
bitRowReduce m@(M r c _) m2 = rowSwaps m m2 0
  where
    {-@ rowSwaps :: m:MatrixS Bit ->
                    m2:{Matrix Bit | mRow m = mRow m2} ->
                    i:{Nat | i < mRow m} ->
                    (MatrixX Bit m, MatrixX Bit m2) /
                    [mRow m - i] @-}
    rowSwaps :: Matrix Bit ->
                Matrix Bit ->
                Int ->
                (Matrix Bit, Matrix Bit)
    rowSwaps m m2 i =
      let (m', m2') = bitRowSwap m m2 i
          (m'', m2'') = rowSums m' m2' i 0 i
          i' = i + 1
      in
        if i' == mRow m
        then (m'', m2'')
        else rowSwaps m'' m2'' i'

    {-@ rowSums :: m:MatrixS Bit ->
                   m2:{Matrix Bit | mRow m = mRow m2} ->
                   h:{Nat | h < mRow m} ->
                   i:{Nat | i < mRow m} ->
                   j:{Nat | j < mCol m} ->
                   (MatrixX Bit m, MatrixX Bit m2) /
                   [mRow m - i] @-}
    rowSums :: Matrix Bit ->
               Matrix Bit ->
               Int ->
               Int ->
               Int ->
               (Matrix Bit, Matrix Bit)
    rowSums m m2 h i j =
      let (m', m2') = bitRowSum m m2 h i j
          i' = i + 1
      in
        if i' == mRow m
        then (m', m2')
        else rowSums m' m2' h i' j

{-@ bitRowSwap :: m:MatrixS Bit ->
                  m2:{Matrix Bit | mRow m = mRow m2} ->
                  i:{Nat | i < mRow m} ->
                  (MatrixX Bit m, MatrixX Bit m2) @-}
bitRowSwap :: Matrix Bit ->
              Matrix Bit ->
              Int ->
              (Matrix Bit, Matrix Bit)
bitRowSwap m m2 i =
  case findRowN m (\row -> (vGet row i) == 1) i of
    Nothing -> (m, m2)
    Just i' ->
      (swapRows m i i', swapRows m2 i i')

{-@ bitRowSum :: m:MatrixS Bit ->
                 m2:{Matrix Bit | mRow m = mRow m2} ->
                 h:{Nat | h < mRow m} ->
                 i:{Nat | i < mRow m && i < mCol m} ->
                 j:{Nat | j < mRow m} ->
                 (MatrixX Bit m, MatrixX Bit m2) @-}
bitRowSum :: Matrix Bit ->
             Matrix Bit ->
             Int ->
             Int ->
             Int ->
             (Matrix Bit, Matrix Bit)
bitRowSum m@(M _ _ rows) m2 h i j = 
  case findRowN m (\row -> row /= (vGet rows h) &&
                           (vGet row j) == 1) i of
    Nothing -> (m, m2)
    Just i' ->
      (addRows m h i', addRows m2 h i')
