{- |
Description : CSCI-788 Capstone Project - Bit type (modulo 2).
Copyright : 2022 Tom Arnold <tca4384@rit.edu>
-}
module Bit where

import List

data Bit = B0 | B1 deriving (Eq, Ord)

-- TODO how should 3::Bit be handled?
instance Num Bit where
  (+) B1 B0 = B1
  (+) B0 B1 = B1
  (+) _ _ = B0
  (*) B1 B1 = B1
  (*) _ _ = B0
  (-) B1 B0 = B1
  (-) B0 B1 = B1
  (-) _ _ = B0
  abs x = x
  signum x = x
  fromInteger 0 = B0
  fromInteger 1 = B1
  fromInteger _ = B0

instance Show Bit where
   show B0 = "0"
   show B1 = "1"

instance Enum Bit where
  toEnum x = if x == 0 then B0 else B1
  fromEnum x = if x == B0 then 0 else 1

instance Real Bit where
  toRational x = if x == B1 then 1 else 0

-- | Implement the integral typeclass so we can use division for bits.
instance Integral Bit where
  quotRem a b = (a * b, 0)
  toInteger a = if a == B0 then 0 else 1

{-@ bitToInt :: b:Bit -> {n:Int | n == 0 || n == 1} @-}
bitToInt :: Bit -> Int
bitToInt B1 = 1
bitToInt B0 = 0
